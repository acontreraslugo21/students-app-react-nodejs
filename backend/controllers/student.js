'use strict'

var path = require('path');
var fs = require('fs');
var Student = require('../models/student');
var Notes = require('../models/notes');

function getStudent(req, res){
    var id = req.params.id;

    Student.findById(id, (err, student) => {
        if(err){
            res.status(500).send({
                message: 'Error en la solicitud'
            });
        }else{
            if(!student){
                res.status(404).send({
                    message: 'El estudiante no existe'
                });
            } else {
                res.status(200).send({
                    student
                });
            }
        }
    })
}

function getStudents(req, res){
    if(req.params.page){
        var page = req.params.page;
    } else {
        var page = 1;
    }
    var items = 3;
    Student.find().sort('name').paginate(page, items, function(err, students, items){
        if (err){
            res.status(500).send({
                message: 'Ha ocurrido un error en la solicitud'
            });
        } else {
            if (!students){
                res.status(404).send({
                    message: 'No hay estudiantes creados'
                });
            } else {
                return res.status(200).send({
                    total_students: items,
                    students: students
                });
            }
        }
    })
}

function createStudent(req, res){
    var student = new Student();
    var params = req.body;

    student.nombre = params.nombre;
    student.edad = params.edad;
    
    student.save((err, studentStored) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al crear el estudiante'
            });
        }else{
            if(!studentStored){
                res.status(404).send({
                    message: 'El estudiante no ha sido guardado'
                });
            }else{
                res.status(200).send({
                    student: studentStored
                });
            }
        }
    });
}

function updateStudent(req, res){
    var id = req.params.id;
    var update = req.body;
    Student.findByIdAndUpdate(id, update, (err, updateStudent) => {
        if(err){
            res.status(500).send({
                message: 'Error al actualizar el curso'
            });
        } else { 
            if(!updateStudent){
                res.status(404).send({
                    message: 'El estudiante no ha sido actualizado'
                });
            } else { 
                res.status(200).send({
                    student: updateStudent
                });
            }
        }
    })
}

function deleteStudent (req, res){
    var id = req.params.id;

    Student.findByIdAndRemove(id, (err, studentRemoved) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al eliminar el estudiante'
            });
        } else {
            if(!studentRemoved){
                res.status(404).send({
                    message: 'El estudiante no ha sido eliminado'
                });
            }else{
                Notes.find({student: studentRemoved._id}).remove((err, noteRemoved) => {
                    if(err){
                        res.status(500).send({
                            message: 'Error al eliminar la nota'
                        });
                    } else {
                        if(!noteRemoved){
                            res.status(404).send({
                                message: 'La nota no ha sido eliminada'
                            });
                        } else {
                            res.status(200).send({
                                student: studentRemoved
                            });
                        }
                    }
                });
            }
        }
    });
}

module.exports = {
    getStudent,
    getStudents,
    createStudent,
    updateStudent,
    deleteStudent
};