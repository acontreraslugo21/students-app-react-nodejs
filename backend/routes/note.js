'use strict'

var express = require('express');
var api = express.Router();
var mdAuth = require('../middlewares/authorization');

var noteController = require('../controllers/notes');

api.get('/nota/:id', mdAuth.auth, noteController.getNote);
api.get('/notas/:page?', mdAuth.auth, noteController.getNotes);
api.post('/crear-nota', mdAuth.auth, noteController.createNote);
api.put('/editar-nota/:id', mdAuth.auth, noteController.updateNote);
api.delete('/nota/:id', mdAuth.auth, noteController.deleteNote);

module.exports = api;
