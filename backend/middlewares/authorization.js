'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'secret_pass';

exports.auth = function(req, res, next){
    if(!req.headers.authorization){
        return res.status(403).send({
            message: 'No se ha recibido autenticación'
        });
    }


    var token = req.headers.authorization.replace(/['"]+/g, '');

    try{
        var payLoad = jwt.decode(token, secret);

        if(payLoad.ex <= moment().unix()){
            console.log(ex);
            return res.status(401).send({
                message: 'El token ha expirado'
            });
        }


    }catch(ex){
        console.log(ex);
        return res.status(403).send({
            message: 'Token inválido'
        });
    }

    req.user = payLoad;

    next();
};

