'use strict'

var express = require('express');
var api = express.Router();
var mdAuth = require('../middlewares/authorization');

var userController = require('../controllers/user');

api.post('/registro', userController.registerUser);
api.post('/login', userController.loginUser);
api.put('/edit-user/:id', mdAuth.auth, userController.updateUser);

module.exports = api;
