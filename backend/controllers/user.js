'use strict'
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var jwt = require('../services/jwt');

var User = require('../models/user');

function registerUser(req, res){
    var user = new User();
    var params = req.body;

    user.nombre = params.nombre;
    user.apellido = params.apellido;
    user.email = params.email;
    user.rol = 'ADMIN';
    
    if(params.password){
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(params.password, salt, function(err, hash) {
                user.password = hash;
                if(user.nombre != null && user.apellido != null && user.email != null){
                    user.save((err, userStored) => {
                        if(err){
                            res.status(500).send({
                                message: 'No se ha creado el usuario'
                            });
                        } else {
                            if(!userStored){
                                res.status(404).send({
                                    message: 'No se ha creado el usuario'
                                });
                            } else {
                                res.status(200).send({
                                    user: userStored
                                });
                            }
                        }
                    })
                } else {
                    res.status(200).send({
                        message: 'Todos los campos son requeridos'
                    });
                }
            });
        });
    } else {
        res.status(200).send({
            message: 'La contraseña es requerida'
        });
    }
}

function loginUser(req, res){
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({email: email.toLowerCase()}, (err, user) => {
        if(err){
            res.status(500).send({
                message: 'Hubo un error, intenta de nuevo'
            });
        } else {
            if(!user){
                res.status(404).send({
                    message: 'El usuario no existe'
                });
            } else {
                bcrypt.compare(password, user.password, function(err, check){
                    if(check){
                        if(params.getHash){
                            res.status(200).send({
                                token:  jwt.createToken(user)
                            });
                        } else {
                            res.status(200).send({
                                user
                            });
                        }
                    } else {
                        res.status(404).send({
                            message: 'Contraseña incorrecta'
                        });
                    }
                });
            }
        }
    });
}



function updateUser(req, res){
    var id = req.params.id;
    var update = req.body;
    User.findByIdAndUpdate(id, update, (err, userUpdated) => {
        if(err){
            res.status(500).send({
                message: 'Error al actualizar el usuario'
            });
        } else { 
            if(!userUpdated){
                res.status(404).send({
                    message: 'El usuario no ha sido actualizado'
                });
            } else { 
                res.status(200).send({
                    user: userUpdated
                });
            }
        }
    })
}

module.exports = {
    registerUser,
    loginUser,
    updateUser
};