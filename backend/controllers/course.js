'use strict'

var path = require('path');
var fs = require('fs');
var mongoosePagination = require('mongoose-pagination');
var Course = require('../models/course');

function getCourse(req, res){
    var id = req.params.id;
    Course.findById(id, (err, course) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error en la solicitud'
            });
        } else {
            if (!course){
                res.status(404).send({
                    message: 'No se encuentra el curso'
                });
            } else {
                return res.status(200).send({
                    course
                });
            }
        }
    });
}

function getCourses(req, res){
    if(req.params.page){
        var page = req.params.page;
    } else {
        var page = 1;
    }
    var items = 3;
    Course.find().sort('name').paginate(page, items, function(err, courses, items){
        if (err){
            res.status(500).send({
                message: 'Ha ocurrido un error en la solicitud'
            });
        } else {
            if (!courses){
                res.status(404).send({
                    message: 'No hay cursos creados'
                });
            } else {
                return res.status(200).send({
                    total_courses: items,
                    courses: courses
                });
            }
        }
    })
}

function createCourse(req, res){
    var course = new Course();
    var params = req.body;

    course.nombre = params.nombre;
    
    course.save((err, courseStored) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al crear el curso'
            });
        }else{
            if(!courseStored){
                res.status(404).send({
                    message: 'El curso no ha sido guardado'
                });
            }else{
                res.status(200).send({
                    course: courseStored
                });
            }
        }
    });
}

function updateCourse(req, res){
    var id = req.params.id;
    var update = req.body;
    Course.findByIdAndUpdate(id, update, (err, courseUpdated) => {
        if(err){
            res.status(500).send({
                message: 'Error al actualizar el curso'
            });
        } else { 
            if(!courseUpdated){
                res.status(404).send({
                    message: 'El curso no ha sido actualizado'
                });
            } else { 
                res.status(200).send({
                    course: courseUpdated
                });
            }
        }
    })
}

function deleteCourse (req, res){
    var id = req.params.id;

    Course.findByIdAndRemove(id, (err, courseRemoved) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al eliminar el curso'
            });
        } else {
            if(!courseRemoved){
                res.status(404).send({
                    message: 'El curso no ha sido eliminado'
                });
            }else{
                res.status(200).send({
                    student: courseRemoved
                });
            }
        }
    });
}

module.exports = {
    createCourse,
    updateCourse,
    getCourse,
    getCourses,
    deleteCourse
};