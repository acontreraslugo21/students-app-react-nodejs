'use strict'

var express = require('express');
var api = express.Router();
var mdAuth = require('../middlewares/authorization');

var studentController = require('../controllers/student');

api.get('/estudiantes/:page?', mdAuth.auth, studentController.getStudents);
api.get('/estudiante/:id', mdAuth.auth, studentController.getStudent);
api.post('/crear-estudiante', mdAuth.auth, studentController.createStudent);
api.put('/editar-estudiante/:id', mdAuth.auth, studentController.updateStudent);
api.delete('/estudiante/:id', mdAuth.auth, studentController.deleteStudent);

module.exports = api;
