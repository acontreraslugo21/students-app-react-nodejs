'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'secret_pass';

exports.createToken = function(user){
    var payLoad = {
        sub: user._id,
        nombre: user.nombre,
        apellido: user.apellido,
        email: user.email,
        role: user.role,
        iat: moment().unix(),
        expiration_date: moment().add(1, 'days').unix()
    }

    return jwt.encode(payLoad, secret);
};

