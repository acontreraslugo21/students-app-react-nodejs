'use strict'

var path = require('path');
var fs = require('fs');
var Note = require('../models/notes');

function getNote(req, res){
    var id = req.params.id;
    Note.findById(id, (err, note) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error en la solicitud'
            });
        } else {
            if (!note){
                res.status(404).send({
                    message: 'No se encuentra la nota'
                });
            } else {
                return res.status(200).send({
                    note
                });
            }
        }
    });
}


function getNotes(req, res){
    if(req.params.page){
        var page = req.params.page;
    } else {
        var page = 1;
    }
    var items = 3;
    Note.find().sort('name').paginate(page, items, function(err, notes, items){
        if (err){
            res.status(500).send({
                message: 'Ha ocurrido un error en la solicitud'
            });
        } else {
            if (!notes){
                res.status(404).send({
                    message: 'No hay notas creadas'
                });
            } else {
                return res.status(200).send({
                    total_notes: items,
                    notes: notes
                });
            }
        }
    })
}

function createNote(req, res){
    var note = new Note();
    var params = req.body;

    note.IdEstudiante = params.IdEstudiante;
    note.IdCurso = params.IdCurso;
    note.NombreEvaluacion = params.NombreEvaluacion;
    note.Calificacion = params.Calificacion;
    
    note.save((err, noteStored) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al crear la nota'
            });
        }else{
            if(!noteStored){
                res.status(404).send({
                    message: 'La nota no ha sido guardada'
                });
            }else{
                res.status(200).send({
                    note: noteStored
                });
            }
        }
    });
}

function updateNote(req, res){
    var id = req.params.id;
    var update = req.body;
    Note.findByIdAndUpdate(id, update, (err, noteUpdated) => {
        if(err){
            res.status(500).send({
                message: 'Error al actualizar el curso'
            });
        } else { 
            if(!noteUpdated){
                res.status(404).send({
                    message: 'La nota no ha sido actualizada'
                });
            } else { 
                res.status(200).send({
                    note: noteUpdated
                });
            }
        }
    })
}

function deleteNote (req, res){
    var id = req.params.id;

    Note.findByIdAndRemove(id, (err, noteRemoved) => {
        if(err){
            res.status(500).send({
                message: 'Ha ocurrido un error al eliminar la nota'
            });
        } else {
            if(!noteRemoved){
                res.status(404).send({
                    message: 'La nota ha sido eliminada'
                });
            }else{
                res.status(200).send({
                    note: noteRemoved
                });
            }
        }
    });
}

module.exports = {
    getNote,
    getNotes,
    createNote,
    updateNote,
    deleteNote
};