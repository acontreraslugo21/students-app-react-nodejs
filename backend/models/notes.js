'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notaSchema = Schema({
    IdEstudiante: String,
    IdCurso: String,
    NombreEvaluacion: String,
    Calificacion: String,
});

module.exports = mongoose.model('Nota', notaSchema);