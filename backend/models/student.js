'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var estudianteSchema = Schema({
    nombre: String,
    edad: String
});

module.exports = mongoose.model('Estudiante', estudianteSchema);