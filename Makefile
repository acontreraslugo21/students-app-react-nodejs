build-backend:
	cd backend && npm install express bcryptjs body-parser jwt-simple moment mongoose mongoose-pagination --save && npm install nodemon --save-dev

start-backend:
	cd backend && npm start

start-app:
	cd studentsapp && yarn start