'use strict'

var express = require('express');
var api = express.Router();
var mdAuth = require('../middlewares/authorization');

var courseController = require('../controllers/course');

api.get('/curso/:id', mdAuth.auth, courseController.getCourse);
api.get('/cursos/:page?', mdAuth.auth, courseController.getCourses);
api.post('/crear-curso', mdAuth.auth, courseController.createCourse);
api.put('/editar-curso/:id', mdAuth.auth, courseController.updateCourse);
api.delete('/curso/:id', mdAuth.auth, courseController.deleteCourse);

module.exports = api;
