'use strict'

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

//Routes
var userRoutes = require('./routes/user');
var studentRoutes = require('./routes/student');
var courseRoutes = require('./routes/course');
var noteRoutes = require('./routes/note');
app.use(bodyParser.urlencoded());

// Convert to JSON object the data
app.use(bodyParser.json());

//Base Routes
app.use('/api', userRoutes);
app.use('/api', studentRoutes);
app.use('/api', courseRoutes);
app.use('/api', noteRoutes);

module.exports = app;


