'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT = 3103;

mongoose.connect('mongodb://localhost:27017/studentsdb', (err, res) => {
    if(err){
        throw err;
    } else {
        console.log('Base de datos está corriendo.');
        app.listen(port, function(){
            console.log('El servidor está escuchando en la url http://localhost:'+ port);
        });
    }
});